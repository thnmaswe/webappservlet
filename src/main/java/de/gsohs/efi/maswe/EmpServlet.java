package de.gsohs.efi.maswe;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

public class EmpServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		Statement stmt;
		Connection con;
		try {
			// get the Directory context of the JNDI Directory of
			// the AppServer (no credentials required)
			DirContext ctx = new InitialDirContext();
			// lookup the Datasource via path in JNDI
			DataSource ds = (DataSource) ctx.lookup("jdbc/hrDS");
			// request a database connection
			con = ds.getConnection();
			// prepare a statement to run a query against the db
			stmt = con.createStatement();
			// executes the SELECT and returns a Cursor Object to
			// iterate over the results
			ResultSet rs = stmt.executeQuery(
					"select e.first_name,e.last_name,e.email,e.phone_number," +
					"d.department_name " +
					"from employees e, departments d " +
					"where e.department_id = d.department_id " +
					"order by e.last_name"
			);
			
			// retrieve the stream in order to print out the webpage
			ServletOutputStream os = res.getOutputStream();
			os.print("<html><head></head><body>" +
					"<table><tr>" +
					"<th>First Name</th><th>last Name</th>" +
					"<th>Email</th><th>Phone Number</th>" +
					"<th>Department</th></tr>");
			
			// move the cursor to the next position (returns a boolean)
			// whether a next position is there or not
			while(rs.next()) {
				os.print("<tr>");
				os.print("<td>"+rs.getString("FIRST_NAME")+"</td>");
				os.print("<td>"+rs.getString("LAST_NAME")+"</td>");
				os.print("<td>"+rs.getString("EMAIL")+"</td>");
				os.print("<td>"+rs.getString("PHONE_NUMBER")+"</td>");
				os.print("<td>"+rs.getString("DEPARTMENT_NAME")+"</td>");
				os.print("</tr>");
			}
			os.print("</table></body></html>");
			
			// release the resources that are not needed anymore
			rs.close();
			stmt.close();
			con.close();
			os.flush();
			os.close();
		} catch (NamingException | SQLException e) {
			throw new ServletException(e);
		}
		
	}
	
}
